<?php


namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class APIControllerTest extends WebTestCase
{
	public function testNewMember(){
		$client = static::createClient();
		$client->request('POST', '/newMember',
			[
				'name' => 'Ivan',
				'email' => 'darkside482@yandex.ru',
				'phone' => '89197279441',
				'password' => '123123'
			]
		);
		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testCreateCampaign(){
		$client = static::createClient();
		$client->request('POST', 'http://127.0.0.1:8000/newCampaign',
			[
				'name' => 'MyTestCampaign',
			]
		);
		$this->assertContains("error_message", $client->getResponse()->getContent());
	}
}