<?php

namespace App\Controller;

use App\Entity\Members;
use App\Entity\Campaign;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Predis\Client;

class APIController extends AbstractController
{
	/**
	 * @Route("/newMember", name="add_member", methods={"POST"})
	 * @param Request $request
	 * @return Response
	 * @throws Exception
	 */
    public function newMember(Request $request, ValidatorInterface $validator)
    {
    	$name = $request->request->get('name');
    	$email = $request->request->get('email');
    	$phone = $request->request->get('phone');
    	$password = $request->request->get('password');

	    $constraints = new Assert\Collection([
		    'name' => [new Assert\NotBlank],
		    'email' => [new Assert\notBlank],
		    'phone' => [new Assert\notBlank],
		    'password' => [new Assert\notBlank],
	    ]);

	    $data = ['name' => $name, 'email' => $email, 'phone' => $phone, 'password' => $password];
	    if($errors = $this->validateRequest($data, $validator, $constraints)){
		    return new JsonResponse(
			    [
				    'developerMessage'=>$errors,
				    'userMessage'=>$errors,
				    'errorCode'=>'001'
			    ],
			    400
		    );
	    }

    	$entityManager = $this->getDoctrine()->getManager();

    	$member = new Members();
    	$member->setName($data['name']);
    	$member->setEmail($data['email']);
    	$member->setPhone($data['phone']);
    	$member->setPassword(password_hash($data['password'], PASSWORD_BCRYPT));
    	$member->setCreatedAt(new \DateTime());

    	$entityManager->persist($member);
    	$entityManager->flush();

	    $uid = bin2hex(random_bytes(32));
	    $member->setApiToken($uid);

	    $redis = new Client();
	    $redis->set($member->getApiToken(), $member->getId());

    	$output = ['uid'=>$uid];
        return new JsonResponse(
            $output,
	        200
        );
    }
    /**
     * @Route("/newCampaign", name="create_campaign", methods={"POST"})
     */
    public function createCampaign(Request $request, ValidatorInterface $validator){
	    $name = $request->request->get('name');
	    $uid = $request->request->get('uid');

	    $redis = new Client();
	    if(!$id = $redis->get($uid)){
		    return new JsonResponse(
			    [
				    ['error_message'=>'wrong token'],
				    400
			    ]
		    );
	    }

	    $constraints = new Assert\Collection([
		    'name' => [new Assert\NotBlank],
		    'id' => [new Assert\notBlank],
	    ]);

	    $data = ['name' => $name, 'id' => $id];
	    if($errors = $this->validateRequest($data, $validator, $constraints)){
		    return new JsonResponse(
			    [
				    'developerMessage'=>$errors,
				    'userMessage'=>$errors,
				    'errorCode'=>'001'
			    ],
			    400
		    );
	    }


    	$entityManager = $this->getDoctrine()->getManager();

    	$campaign = new Campaign();

    	$campaign->setName($data['name']);
    	$campaign->setMembersId($data['id']);
    	$campaign->setCreatedAt(new \DateTime());

    	$entityManager->persist($campaign);
    	$entityManager->flush();

    	$output = ['id'=>$campaign->getId(),'name'=>$campaign->getName()];
    	return new JsonResponse(
    		$output,
		    200);

    }

	/**
	 * @Route("/getCampaign", name="get_campaign", methods={"GET"})
	 */
    public function getCampaign(Request $request, ValidatorInterface $validator ){
	    $name = $request->query->get('name');
	    $message_end = $request->query->get('message_end');


	    $constraints = new Assert\Collection([
		    'name' => [new Assert\NotBlank],
		    'message_end' => [new Assert\notBlank],
	    ]);

	    $data = ['name' => $name, 'message_end' => $message_end];
	    if($errors = $this->validateRequest($data, $validator, $constraints)){
		    return new JsonResponse(
			    [
				    'developerMessage'=>$errors,
				    'userMessage'=>$errors,
				    'errorCode'=>'001'
			    ],
			    400
		    );
	    }
	    
	    $entityManager = $this->getDoctrine()->getManager();

	    $campaign = $entityManager
		    ->getRepository(Campaign::class)
		    ->findOneBy([
		    	'name'=>$data['name'],
			    'message_end'=>$data['message_end']
		    ]);

	    $output = [
		    'id'=>$campaign->getId(),
		    'name'=>$campaign->getName(),
		    'message_end' => $campaign->getMessageEnd(),
		    'created_at' => $campaign->getCreatedAt()
		    ];
	    return new JsonResponse(
		    $output,
		    200);
    }

	/**
	 * @Route("/editCampaign", name="edit_campaign", methods={"POST"})
	 */
    public function editCampaign(Request $request, ValidatorInterface $validator){
	    $name = $request->request->get('name');
	    $message_end = $request->request->get('message_end');

	    $constraints = new Assert\Collection([
		    'name' => [new Assert\NotBlank],
		    'message_end' => [new Assert\notBlank],
	    ]);

	    $data = ['name' => $name, 'message_end' => $message_end];
	    if($errors = $this->validateRequest($data, $validator, $constraints)){
		    return new JsonResponse(
			    [
				    'developerMessage'=>$errors,
				    'userMessage'=>$errors,
				    'errorCode'=>'001'
			    ],
			    400
		    );
	    }

	    $entityManager = $this->getDoctrine()->getManager();

	    $campaign = $entityManager
		    ->getRepository(Campaign::class)
		    ->findOneBy(['name'=>$data['name']]);
	    $campaign->setMessageEnd($data['message_end']);

	    $entityManager->flush();

	    $output = [
	    	'id'=>$campaign->getId(),
		    'name'=>$campaign->getName(),
		    'message_end' => $campaign->getMessageEnd()];
	    return new JsonResponse(
		    $output,
		    200);
    }

	/**
	 * @Route("/deleteCampaign", name="delete_campaign", methods={"POST"})
	 */
	public function removeCampaign(Request $request, ValidatorInterface $validator){
		$id = $request->request->get('id');

		$constraints = new Assert\Collection([
			'id' => [new Assert\NotBlank],
		]);

		$data = ['id' => $id];
		if($errors = $this->validateRequest($data, $validator, $constraints)){
			return new JsonResponse(
				[
					'status'=>'400',
					'developerMessage'=>$errors,
					'userMessage'=>$errors,
					'errorCode'=>'001'
				]
			);
		}

		$entityManager = $this->getDoctrine()->getManager();

		$campaign = $entityManager
			->getRepository(Campaign::class)
			->find($data);

		$entityManager->remove($campaign);
		$entityManager->flush();

		$output = [
			'status'=>'ok',
		];
		return new JsonResponse(
			$output,
			200);
	}

	/**
	 * @Route("/deleteMember", name="delete_member", methods={"POST"})
	 */
	public function removeMember(Request $request){
		$uid = $request->request->get('uid');

		$redis = new Client();
		if(!$id = $redis->get($uid)){
			return new JsonResponse(
					['error_message'=>'wrong token'],
				400
			);
		}

		$entityManager = $this->getDoctrine()->getManager();

		$campaign = $entityManager
			->getRepository(Members::class)
			->find($id);

		$entityManager->remove($campaign);
		$entityManager->flush();

		$output = [
			'status'=>'ok',
		];
		return new JsonResponse(
			$output,
			200);
	}

	private function validateRequest($data, $validator, $constraints){
		$violations = $validator->validate($data, $constraints);

		if (count($violations) > 0) {

			$accessor = PropertyAccess::createPropertyAccessor();

			$errorMessages = [];

			foreach ($violations as $violation) {

				$accessor->setValue($errorMessages,
					$violation->getPropertyPath(),
					$violation->getMessage());
			}

			return $errorMessages;

		}
		return false;
	}

}
